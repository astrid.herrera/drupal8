<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/igac/templates/system/page.html.twig */
class __TwigTemplate_4adbb40a535ed47bc6f164d5e11bb4dfefdd4502b12c65e7e936a66a736db254 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
            'main' => [$this, 'block_main'],
            'sidebar_first' => [$this, 'block_sidebar_first'],
            'highlighted' => [$this, 'block_highlighted'],
            'help' => [$this, 'block_help'],
            'content' => [$this, 'block_content'],
            'sidebar_second' => [$this, 'block_sidebar_second'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["include" => 1, "block" => 4, "if" => 9, "set" => 19];
        $filters = ["escape" => 5];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['include', 'block', 'if', 'set'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        $this->loadTemplate((($context["directory"] ?? null) . "/templates/includes/header.twig.yml"), "themes/igac/templates/system/page.html.twig", 1)->display($context);
        // line 2
        echo "
";
        // line 4
        $this->displayBlock('main', $context, $blocks);
        // line 60
        echo "


";
        // line 63
        $this->loadTemplate((($context["directory"] ?? null) . "/templates/includes/footer.twig.yml"), "themes/igac/templates/system/page.html.twig", 63)->display($context);
    }

    // line 4
    public function block_main($context, array $blocks = [])
    {
        // line 5
        echo "  <div role=\"main\" class=\"container ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null)), "html", null, true);
        echo " js-quickedit-main-content\">
    <div class=\"row\">
   
      ";
        // line 9
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])) {
            // line 10
            echo "        ";
            $this->displayBlock('sidebar_first', $context, $blocks);
            // line 15
            echo "      ";
        }
        // line 16
        echo "
      ";
        // line 18
        echo "      ";
        // line 19
        $context["content_classes"] = [0 => ((($this->getAttribute(        // line 20
($context["page"] ?? null), "sidebar_first", []) && $this->getAttribute(($context["page"] ?? null), "sidebar_second", []))) ? ("col-sm-6") : ("")), 1 => ((($this->getAttribute(        // line 21
($context["page"] ?? null), "sidebar_first", []) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])))) ? ("col-sm-9") : ("")), 2 => ((($this->getAttribute(        // line 22
($context["page"] ?? null), "sidebar_second", []) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])))) ? ("col-sm-9") : ("")), 3 => (((twig_test_empty($this->getAttribute(        // line 23
($context["page"] ?? null), "sidebar_first", [])) && twig_test_empty($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])))) ? ("col-sm-12") : (""))];
        // line 26
        echo "      <section";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["content_attributes"] ?? null), "addClass", [0 => ($context["content_classes"] ?? null)], "method")), "html", null, true);
        echo ">

        ";
        // line 29
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "highlighted", [])) {
            // line 30
            echo "          ";
            $this->displayBlock('highlighted', $context, $blocks);
            // line 33
            echo "        ";
        }
        // line 34
        echo "
        ";
        // line 36
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "help", [])) {
            // line 37
            echo "          ";
            $this->displayBlock('help', $context, $blocks);
            // line 40
            echo "        ";
        }
        // line 41
        echo "
        ";
        // line 43
        echo "        ";
        $this->displayBlock('content', $context, $blocks);
        // line 47
        echo "      </section>

      ";
        // line 50
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])) {
            // line 51
            echo "        ";
            $this->displayBlock('sidebar_second', $context, $blocks);
            // line 56
            echo "      ";
        }
        // line 57
        echo "    </div>
  </div>
";
    }

    // line 10
    public function block_sidebar_first($context, array $blocks = [])
    {
        // line 11
        echo "          <aside class=\"col-sm-3\" role=\"complementary\">
            ";
        // line 12
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_first", [])), "html", null, true);
        echo "
          </aside>
        ";
    }

    // line 30
    public function block_highlighted($context, array $blocks = [])
    {
        // line 31
        echo "            <div class=\"highlighted\">";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "highlighted", [])), "html", null, true);
        echo "</div>
          ";
    }

    // line 37
    public function block_help($context, array $blocks = [])
    {
        // line 38
        echo "            ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "help", [])), "html", null, true);
        echo "
          ";
    }

    // line 43
    public function block_content($context, array $blocks = [])
    {
        // line 44
        echo "          <a id=\"main-content\"></a>
          ";
        // line 45
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
        echo "
        ";
    }

    // line 51
    public function block_sidebar_second($context, array $blocks = [])
    {
        // line 52
        echo "          <aside class=\"col-sm-3\" role=\"complementary\">
            ";
        // line 53
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_second", [])), "html", null, true);
        echo "
          </aside>
        ";
    }

    public function getTemplateName()
    {
        return "themes/igac/templates/system/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  210 => 53,  207 => 52,  204 => 51,  198 => 45,  195 => 44,  192 => 43,  185 => 38,  182 => 37,  175 => 31,  172 => 30,  165 => 12,  162 => 11,  159 => 10,  153 => 57,  150 => 56,  147 => 51,  144 => 50,  140 => 47,  137 => 43,  134 => 41,  131 => 40,  128 => 37,  125 => 36,  122 => 34,  119 => 33,  116 => 30,  113 => 29,  107 => 26,  105 => 23,  104 => 22,  103 => 21,  102 => 20,  101 => 19,  99 => 18,  96 => 16,  93 => 15,  90 => 10,  87 => 9,  80 => 5,  77 => 4,  73 => 63,  68 => 60,  66 => 4,  63 => 2,  61 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/igac/templates/system/page.html.twig", "/var/www/html/igac/themes/igac/templates/system/page.html.twig");
    }
}
