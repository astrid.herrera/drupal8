<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/igac/templates/includes/header.twig.yml */
class __TwigTemplate_cbb4d44a9a085a9e778649088b6c7f17a579c433550a7aabad05b72add6f8453 extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 33];
        $filters = ["t" => 2, "escape" => 10];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if'],
                ['t', 'escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 1
        echo "
<header id=\"header\" class=\"header\" role=\"banner\" aria-label=\"";
        // line 2
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Site header"));
        echo "\">

                    <div class=\"container\">
                            <div class=\"row\">
                                <div class=\"col-md-3\">
                                 <p>MARTES, 12 DE NOVIEMBRE DE 2019</p>
                                </div>
                                <div class=\"col-md-3\">
                                  ";
        // line 10
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "finder", [])), "html", null, true);
        echo "
                               </div>
                                 <div class=\"col-md-3\">
                                 ";
        // line 13
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "top_menu", [])), "html", null, true);
        echo "
                               </div>
                                 <div class=\"col-md-3\">
                                 ";
        // line 16
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "gtranslate", [])), "html", null, true);
        echo "
                               </div>
                            </div>
                             <div class=\"row\">
                                <div class=\"col-md-8\">
                                       ";
        // line 21
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header1", [])), "html", null, true);
        echo "
                                </div>                            
                                 <div class=\"col-md-4\">
                                 <img src=\"/themes/igac/assets/images/logo2.png\">
                               </div>
                               
                            </div>
        
      
                <div class=\"row\">
                
                    <div class=\"col-md-12\">
                            ";
        // line 33
        if ( !$this->getAttribute(($context["navbar_attributes"] ?? null), "hasClass", [0 => ($context["container"] ?? null)], "method")) {
            // line 34
            echo "                    <div class=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["container"] ?? null)), "html", null, true);
            echo "\">
                    ";
        }
        // line 36
        echo "                    
                  <div class=\"navbar-header\">
                    ";
        // line 38
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "navigation", [])), "html", null, true);
        echo "
                    ";
        // line 40
        echo "                    ";
        if ($this->getAttribute(($context["page"] ?? null), "navigation_collapsible", [])) {
            // line 41
            echo "                      <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\"#navbar-collapse\">
                        <span class=\"sr-only\">";
            // line 42
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Toggle navigation"));
            echo "</span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                      </button>
                    ";
        }
        // line 48
        echo "                  </div>

                  ";
        // line 51
        echo "                  ";
        if ($this->getAttribute(($context["page"] ?? null), "navigation_collapsible", [])) {
            // line 52
            echo "                  
                  <div id=\"navbar-collapse\" class=\"collapse navbar-collapse\" data-hover=\"dropdown\" data-animations=\"fadeInDown fadeInRight fadeInUp fadeInLeft\">
                      ";
            // line 54
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "navigation_collapsible", [])), "html", null, true);
            echo "
                    </div>
                  ";
        }
        // line 57
        echo "                  ";
        if ( !$this->getAttribute(($context["navbar_attributes"] ?? null), "hasClass", [0 => ($context["container"] ?? null)], "method")) {
            // line 58
            echo "                    </div>
                  ";
        }
        // line 60
        echo "                    </div>
              
        </div>\t       
    </div>
</header>";
    }

    public function getTemplateName()
    {
        return "themes/igac/templates/includes/header.twig.yml";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  159 => 60,  155 => 58,  152 => 57,  146 => 54,  142 => 52,  139 => 51,  135 => 48,  126 => 42,  123 => 41,  120 => 40,  116 => 38,  112 => 36,  106 => 34,  104 => 33,  89 => 21,  81 => 16,  75 => 13,  69 => 10,  58 => 2,  55 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/igac/templates/includes/header.twig.yml", "/var/www/html/igac/themes/igac/templates/includes/header.twig.yml");
    }
}
